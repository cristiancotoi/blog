---
layout: page
title: CV
permalink: /cv/
---

# Experience

I started my career as a manual tester @ Bitdefender. That was 13 years ago.
Since then I gradually switched to automated testing,
to development and now I'm an architect @Endava.
This colorful and vast experience enables
a deeper understanding of software products
from both technical and business perspectives.

## Senior Software Architect @Endava

While not an architect specific task, at Endava I am also leading a team
and currently we are working on developing software for smart speakers. 