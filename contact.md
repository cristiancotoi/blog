---
layout: page
title: Contact
permalink: /contact/
---

You can reach me our on social media, but if you want something more direct
you can contact me directly by using [this form](http://bit.ly/1E50SDS).
