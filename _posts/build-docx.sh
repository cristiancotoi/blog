#!/usr/bin/env bash

# docx destination
# commit the result of the command to git, so the business users can pull the latest version
mkdir -p docx

pandoc \
    --from markdown \
    --to docx \
    --output docx/automated-testing.docx \
    \
    01-introduction.md \
    01-git.md \
    01-selenium.md \
    \
    02-00-environment-setup.md \
    02-01-java-basics-foundation.md \
    02-02-java-basics-strings.md \
    02-03-java-basics-decisions.md \
    02-04-java-basics-collections.md \
    02-05-java-basics-set-map.md \
    02-10-java-basics-oop.md \
    02-11-java-basics-interfaces.md \
    02-12-java-basics-date-and-time.md \
    02-13-java-basics-random-data.md \
    \
    03-00-junit-and-hamcrest.md \
    03-01-selenium-interrogation.md \
    03-02-selenium-manipulation.md \
    03-03-selenium-advanced-operations.md \
    03-04-selenium-synchronization.md \
    09-selenium-recap.md \
    \
    04-database-testing.md \
    05-01-api-tesitng-introduction.md \
    05-02-api-testing-rest-assured-intro.md \
    05-03-api-testing-object-mapping.md \
    \
    06-01-page-object-modelling.md \
    06-02-page-object-page-factory.md \
    \
    07-data-driven-testing.md \
    \
    08-01-extra-working-with-files.md \
    08-02-extra-logging.md \
    08-03-extra-screenshots.md \
    08-04-extra-testng-vs-junit.md \
    08-05-extra-serenity.md
