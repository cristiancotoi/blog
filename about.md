---
layout: page
title: About
permalink: /about/
---

My name is Cristian Coțoi and I'm a software architect.

This is my personal site, with a CV and 
some thoughts thrown around in my blog.
